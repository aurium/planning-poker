"use strict";

const DEBUG = document.location.search === '?debug';

var cards = []
var gradDefault = ['#33AACC','#CC44CC'], grad = gradDefault
var storageGradKey = 'planning-poker-gradient'

if (localStorage[storageGradKey]) {
    grad = JSON.parse(localStorage[storageGradKey])
}

window.addEventListener('card-loaded', mkCards)

function mkCards() {
    ['?', '½', 0, 1, 2, 3, 5, 8].forEach((value, i)=> {
        let card = mkCardFront(value, grad, table)
        cards.push(card)
        card.addEventListener('click', toggleSelectCard)
    })
    configUI()
    table.addEventListener('click', init)
}

function configUI() {
    var el = window // document.body
    el.addEventListener('mousedown',   mayStartDrag)
    el.addEventListener('mouseup',     mustStopDrag)
    el.addEventListener('mousemove',   mayDrag)
    el.addEventListener('touchstart',  mayStartDrag)
    el.addEventListener('touchend',    mustStopDrag)
    el.addEventListener('touchcancel', mustStopDrag)
    el.addEventListener('touchmove',   mayDrag)

    document.querySelectorAll('#uiDefGrad input').forEach((input, i)=> {
        input.value = grad[i]
        input.addEventListener('change', updateGrad)
    })
    updateGradBts()
}

function updateGrad() {
    document.querySelectorAll('#uiDefGrad input').forEach((input, i)=> {
        grad[i] = input.value
        input.parentNode.style.background = input.value
    })
    updateGradBts()
    localStorage[storageGradKey] = JSON.stringify(grad)
    document.querySelectorAll('#table .card').forEach((baseEl)=> {
        baseEl.style.backgroundImage = `linear-gradient(45deg,${grad.join(',')})`
    })
}

function updateGradBts() {
    document.querySelectorAll('#uiDefGrad label').forEach((label, i)=> {
        label.style.background = grad[i]
    })
}

function setDefaultGrad() {
    document.querySelectorAll('#uiDefGrad input').forEach((input, i)=> {
        input.value = gradDefault[i]
    })
    updateGrad()
}

function init() {
    cards.forEach((card, i)=>
        setTimeout(()=> card.classList.add('open'), i*200+500)
    )

    //if (!DEBUG) document.documentElement.requestFullscreen()
    //.then(()=> console.log('Fullscreen!') )
    //.catch(console.error)
}

function toggleSelectCard() {
    let isSel = this.classList.contains('selected')
    cards.forEach((card)=> card.classList.remove('selected') )
    if (!isSel) this.classList.add('selected')
}

var dragCtrlStartY = null, ctrlIsOpen = false

function mayStartDrag(ev) {
    if (ctrlIsOpen) return null
    var touch = ev.changedTouches ? ev.changedTouches[0] : ev
    if (!dragCtrlStartY && touch.pageY < window.innerHeight/2) dragCtrlStartY = touch.pageY
    document.body.classList.add('opening-ctrl')
}

function mustStopDrag() {
    dragCtrlStartY = null
    document.body.classList.remove('opening-ctrl')
    ctrl.style.height = null
}

function mayDrag(ev) {
    if (!dragCtrlStartY) return null
    var touch = ev.changedTouches ? ev.changedTouches[0] : ev
    var halfHeight = window.innerHeight/2
    var delta = touch.pageY-dragCtrlStartY
    if (delta < halfHeight) {
        var height = Math.round(delta)
        document.body.classList.remove('ctrl-open')
        ctrlIsOpen = false
    } else {
        var height = Math.round( (halfHeight + delta) / 2 )
        document.body.classList.add('ctrl-open')
        ctrlIsOpen = true
    }
    ctrl.style.height = height + 'px'
}

function closeCtrl() {
    ctrlIsOpen = false
    document.body.classList.remove('ctrl-open')
}
