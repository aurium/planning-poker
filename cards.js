"use strict";

const twoDig = (num)=> `${ num<10 ? '0' : '' }${ num }`
const rnd = ()=> Math.random().toString(16).split('.')[1]
var rawSvg

window.addEventListener('DOMContentLoaded', ()=> fetch('carta.svg')
    .then((response)=> {
        if (!response.ok) throw new Error('HTTP error ' + response.status)
        return response.text()
    })
    .then((svgCode)=> {
        rawSvg = svgCode.replace(/<\?.*\?>/, '')
        window.dispatchEvent(new Event('card-loaded'))
    })
    .catch((err)=> {
        console.error(err)
        alert(err.message)
    })
)

function mkCardFront(value, grad, place) {
    const baseEl = document.createElement('div')
    const miolo = document.createElement('img')
    const conten = document.createElement('div')
    const valTop = document.createElement('div')
    const valBot = document.createElement('div')
    baseEl.className = `card card-${value}`
    conten.className = `content`
    valTop.className = `val top`
    valBot.className = `val bot`
    miolo.className = `miolo`
    place.appendChild(baseEl)
    baseEl.appendChild(miolo)
    baseEl.appendChild(valTop)
    baseEl.appendChild(valBot)
    valTop.innerText = value
    valBot.innerText = value
    baseEl.style.backgroundImage = `linear-gradient(45deg,${grad.join(',')})`
    let id = rnd()
    miolo.src = 'data:image/svg+xml;base64,' + btoa(
        updateSVG(rawSvg, value)
        .replace(/(id="mask(-group)?)"/g, `$1-${id}"`)
        .replace(/url\(#mask\)/g, `url(#mask-${id})`)
        .replace(/href="#mask-group"/g, `href="#mask-group-${id}"`)
    )
    return baseEl
}

function updateSVG(svg, value) {
    var rmDots = (...dots)=> {
        dots.map((x)=> new RegExp(`(id="dot-${x}")`) )
        .forEach((re)=> svg = svg.replace(re, '$1 visibility="hidden"') )
    }
    var ventos = ['N', 'S', 'L', 'O', 'NL', 'NO', 'SL', 'SO']
    switch (value) {
        case '?':
            rmDots('zero', 'meio', ...ventos, 'centro', 'val1', 'dupla-A', 'dupla-B')
            break;
        case '½':
            rmDots('quest', 'zero', ...ventos, 'centro', 'val1', 'dupla-A', 'dupla-B')
            break;
        case 0:
            rmDots('quest', 'meio', ...ventos, 'centro', 'val1', 'dupla-A', 'dupla-B')
            break;
        case 1:
            rmDots('quest', 'zero', 'meio', ...ventos, 'centro', 'dupla-A', 'dupla-B')
            break;
        case 2:
            rmDots('quest', 'zero', 'meio', ...ventos, 'centro', 'val1')
            break;
        case 3:
            rmDots('quest', 'zero', 'meio', 'N', 'S', 'L', 'O', 'NL', 'SO', 'val1', 'dupla-A', 'dupla-B')
            break;
        case 5:
            rmDots('quest', 'zero', 'meio', 'N', 'S', 'L', 'O', 'dupla-A', 'dupla-B', 'val1')
            break;
        case 8:
            rmDots('quest', 'zero', 'meio', 'centro', 'dupla-A', 'dupla-B', 'val1')
            break;
    }
    return svg
}
