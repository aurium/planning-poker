"use strict";

console.log('SW:', self.constructor.name, Object.keys(self));

console.log('Registration scope:', self.registration.scope);

const root = self.registration.scope.replace(/https?:\/\/[^\/]+|\/$/g, '');
console.log('Root:', root);

self.onmessage = function(msg) { console.log('Handling message event:', messageEvent) }

const cacheName = 'Planning-Poker-nTopus-WPQA';
const files = [
    root+'/cards.css',
    root+'/play.css',

    root+'/cards.js',
    root+'/play.js',

    root+'/carta.svg',
    root+'/cloth.jpg',
    root+'/paper.jpg',

    root+'/esphimere.otf',
    root+'/getvoip-grotesque.ttf',

    root+'/index.html',
    root+'/package.json',
    root+'/site.webmanifest'
];
console.log('Files to cache:', files);

self.addEventListener('install', function(e) {
  console.log('[Service Worker] Installing...');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      console.log('[Service Worker] Caching all: app shell and content');
      return cache.addAll(files);
    })
  );
});

self.addEventListener('fetch', function(e) {
  e.respondWith(
    caches.match(e.request).then(function(r) {
      console.log('[Service Worker] Fetching resource: '+e.request.url);
      return r || fetch(e.request).then(function(response) {
        return caches.open(cacheName).then(function(cache) {
          console.log('[Service Worker] Caching new resource: '+e.request.url);
          cache.put(e.request, response.clone());
          return response;
        });
      });
    })
  );
});
