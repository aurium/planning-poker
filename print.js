window.addEventListener('card-loaded', init)

function init() {
    [ ['#3AC','#C4C'], ['#494','#BC2','#EC2'] ].forEach((grad)=> {
        buildCards(grad)
    })
    mkCardFront(5, ['#3AC','#C4C'], document.getElementById('one-card'))
}

function buildCards(grad) {
    ;['?', '½', 0, 1, 2, 3, 5, 8].forEach((value)=> {
        mkCardFront(value, grad, front)
    })
}

